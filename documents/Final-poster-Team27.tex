\documentclass[25pt, a0paper, landscape]{tikzposter}

%% \font\nullfont=cmr10

\title{SAM: Stress Anxiety Monitor}
\author{Gabriella Armijo and Emlee Taylor-Bowlin}
\date{\today}
\institute{Team 27 - Monte del Sol Charter School}


\usepackage{graphicx}
\usepackage{biblatex}
\usepackage{hyperref}
\usepackage{adjustbox}
\usepackage{multicol}
\usepackage{wrapfig}

\usetheme{Envelope}

\begin{document}

\maketitle

    \begin{columns}
        \column{0.24}
        \block[bodyoffsety=1.25cm, titleoffsety=1.25cm]{Background}
        {
        Suicide is the 2nd leading cause of death among teenagers and young adults worldwide
        (D’Hotman et al., 2020; ``Suicide statistics and facts. SAVE.'', 2021). New Mexico is currently
        ranked \#4 in the nation for states with the highest suicide rate. (``America’s Health Rank-
        ings.'' 2020) New Mexico’s current teen suicide rate is 24 deaths per 100,000 adolescents ages
        15--19 per year (``Suicide rates by state 2023.'' 2023). This results in about 513 teen suicides a
        year.

        Suicide is preventable with intervention; however, it is hard to intervene when many teens and young
        adults spend a majority of their time chatting on social media or via text messaging.
        Cries for help may be easily masked in this dull form of communication.

        With the help of machine learning, however, suicide prevention may be easier. We
        created an AI named SAM that is able to go through social media posts and/or text messages
        looking for trigger words that may indicate that a person may be showing signs of high stress
        that may lead to suicide.
        }

        \block[bodyoffsety=1.25cm, titleoffsety=1.25cm]{Project Goal}
        {
        Our goal was to have SAM accurately determine if a person may be at high risk of suicide
        or self-harm based on messages people share online. We hope SAM will lead to the early
        detection and prevention of suicide in things as simple as a text message.
        }
        

        \block[bodyoffsety=1.25cm, titleoffsety=1.25cm]{Model and Methods}
        {
        To begin, we obtained a data set containing 2,343 rows and 116 columns. We
        trimmed the data set to contain all the rows and 4 columns: row number,
        the text, the label, and confidence score. Now our data set looks like this:\\
        \begin{tikzfigure}[Snippet of worrisome message data.]
          \label{fig:snipp-worrisome}
          \includegraphics[width=0.20\textwidth]{matrixpics/DataSnip}
        \end{tikzfigure}

        Our label is a binary label that codes for stress or no stress. A label of ``1'' indicates
        stress, whereas ``0'' does not. These numbers were provided by professionals based on the text
        for that row. Confidence was another professional-provided value that has not been incorporated into our model
        at this point. This data is ready to be fed into the Random Forest Classifier that we wrote.
        }

        \column{0.25}

        \block[bodyoffsety=1.25cm, titleoffsety=1.25cm]{Random Forest Models}
        {
        \normaltext
        Random Forest Classifiers and Regressors are both machine learning methods. Random Forest models take numeric values and feed them into decision trees. The
        trees decide where this data continues and breaks it into two or more trees. Ending up as
        decision leaves that are numerical values and the final output of the decision. Regressors can be a range,
        while classifiers give a binary value. We used a Random Forest Classifier that produces a 1 to indicate stress
        or a 0 to indicate no stress. We created two methods to process the data before it is fed into the model.
        \begin{tikzfigure}

            \includegraphics[scale = .75]{matrixpics/RFClass}

            \caption{ \small Fig. 2: Example of Random Forest Classifier Image borrowed from: \tiny https://www.analyticsfordecisions.com/decision-trees-vs-random-forests/}
        \end{tikzfigure}

        }

        \block[bodyoffsety=1cm, titleoffsety=1cm]{Intensity Score Method}
        {\normaltext
        Our first method, is the Intensity Score method. We wrote a dictionary of ``worrisome''
        keywords. Each word was given a weight, and the weighed word occurrences
        were added. The result was a training set consisting of a single numeric score for each
        text snippet, and the ``expert label'' of stress or no stress for that snippet.}

        \block[bodyoffsety=1cm, titleoffsety=1cm,]{Bag of Words Method}
        {
        \normaltext
        The second method is the Bag of Words method. It reads through the text snippet accounting
        for the total number of times any word from the English dictionary appears. It then scales
        the numbers by importance: the number of times each word occurs. Words such as: the, as, and, if
        will carry less importance not affecting the interpretation. This method is
        more reliable, accounting for all possible words. However, it produces a sparse vector containing
        many values of zero that show no data. It is useful when we see zeros in spots that would
        indicate words like suicide, depression, or lonesomeness instead of words that are not included in the snippet.
        \begin{tikzfigure}
            \includegraphics[scale=.5]{matrixpics/BagofWords}

            \caption{ \small Fig. 3: Example of Bag of Words Imaged borrowed from: \tiny https://ucdavisdatalab.github.io/workshop-nlp-healthcare/working-with-text-data-in-r.html }
        \end{tikzfigure}
        }
        
        \column{0.31}

        \block[bodyoffsety=1.25cm, titleoffsety=1.25cm]{Results}
        {
        Our final model accurately predicts if someone is highly stressed based on snippets
        of text. We tested the applicability of our model by feeding it data sets of different sizes, and
        have the Random Forest Classifier print the ``mean absolute error'' (MAE), the ``accuracy score'', and
        the "confusion matrix" performance metrics.
        Our discussion of metrics is based on the ``dreaddit'' training (2838 samples) and testing
        (714 samples) data sets.

        \begin{adjustbox}{width=13in,center}
          \begin{tabular}{||c c || c c||}
            \hline
            in-sample MAE & in-sample accuracy & out-of-sample MAE &
            out-of-sample accuracy \\ [0.5ex]
            \hline\hline
            0.0014 & 0.99859 & 0.30629 & 0.6937 \\
            \hline
          \end{tabular}
        \end{adjustbox}\\
        {\normalsize Fig. 4: SAM results from multiple trials}

        A confusion matrix is a way to express if the classifiers predictions were correct. The confusion matrix
        generates the actual and predicted values. The diagnal elements denote correctly classified outcomes while
        misclassified outcomes are represented in the off-diagonal portions. They are commonly
        denoted as true positive, true negative, false positive, and false negative.
        \begin{wrapfigure}[12]{r}{0.5\linewidth}
          \begin{tikzfigure}
            \includegraphics[scale=0.75]{matrixpics/Examplematrix}
            \caption{Example Confusion Matrix}
          \end{tikzfigure}
        \end{wrapfigure}

        In the confusion matrix to the right, the left value represents the True Negative
        where SAM correctly predicted the person is stressed. The top right value is a False
        Positive where SAM incorrectly predicted that the person was stressed when they
        are OK. The bottom left is the False Negative, SAM incorrectly predicted
        the person to be ok when they are actually stressed – this value is the key to whether our
        method works. The bottom right is a True Positive where SAM correctly predicted
        the person is not stressed. Our model can be ran with either the bag-of-words or the intensity score
        method: the user can switch with a command-line option. Our model compares its predictions
        to the actual results and produces the confusion matrices. For either method, our model
        trained itself on a user specified data set sample before it tested itself on an unknown data set.
        Using both methods, we deduced that the Bag of Words method was more effective in correctly classifying
        if someone was stressed or not.

        \begin{minipage}[b]{0.49\linewidth}
          \centering
          \begin{tikzfigure}[{In-sample Predictions (bag of words)}]
            \includegraphics[scale=.5]{matrixpics/confusion_insample_bag-of-words}
          \end{tikzfigure}
        \end{minipage}
        \begin{minipage}[b]{0.49\linewidth}
          \begin{tikzfigure}[{Out-of-sample Predictions (bag of words)}]
            \centering
            \includegraphics[scale=.5]{matrixpics/confusion_outofsample_bag-of-words}
          \end{tikzfigure}
        \end{minipage}

        %% \begin{tikzfigure}[Training and Testing Predictions]
        %%   \begin{subtikzfigure} % [{\small Training Predictions}]
        %%     \centering
        %%     %% \caption{\small Training Predictions}
        %%     \includegraphics[scale=.5]{matrixpics/confusion_insample_bag-of-words}
        %%   \end{subtikzfigure}
        %%   \begin{subtikzfigure} % [{\small Testing Predictions}]
        %%     \centering
        %%     \includegraphics[scale=.5]{matrixpics/confusion_outofsample_bag-of-words}
        %%     %% \caption{\small Testing Predictions}
        %%   \end{subtikzfigure}
        %% \end{tikzfigure}

        The two figures above show the confusion matrices for the Bag of Words method. In this instance
        the ``in-sample'' comes from the dreaddit training data, and the ``out of sample'' comes from
        the dreaddit test data. The two figures below show the
        confusion matrices results from the ``intensity score''
        approach, used on the same
        data sets.\\
        \begin{minipage}[b]{0.49\linewidth}
          \centering
          \begin{tikzfigure}[{ In-sample Predictions (intensity score)}]
            \includegraphics[scale=.5]{matrixpics/confusion_insample_intensity-score}
          \end{tikzfigure}
        \end{minipage}
        \begin{minipage}[b]{0.49\linewidth}
          \begin{tikzfigure}[{ Out-of-sample Predictions (intensity score)}]
            \centering
            \includegraphics[scale=.5]{matrixpics/confusion_outofsample_intensity-score}
          \end{tikzfigure}
        \end{minipage}

        %% \begin{minipage}[b]{0.49\linewidth}
        %%   \centering
        %%   \begin{tikzfigure}[{\small Training Predictions}]
        %%     \includegraphics[scale=0.5]{matrixpics/confusion_insample_intensity-score}
        %%   \end{tikzfigure}
        %% \end{minipage}
        %% \begin{minipage}[b]{0.49\linewidth}
        %%   \begin{tikzfigure}[{\small Testing Predictions}]
        %%     \centering
        %%     \includegraphics[scale=0.5]{matrixpics/confusion_outofsample_intensity-score}
        %%   \end{tikzfigure}
        %% \end{minipage}


        %% \begin{tikzfigure}[Training and Testing Predictions]
        %%   \begin{subtikzfigure} % [{\small Training Predictions}]
        %%     \centering
        %%     \includegraphics[scale=0.5]{matrixpics/confusion_insample_intensity-score}
        %%     %% \caption{\small Training Predictions}
        %%   \end{subtikzfigure}
        %%   \begin{subtikzfigure} % [{\small Testing Predictions}]
        %%     \centering
        %%     \includegraphics[scale=0.5]{matrixpics/confusion_outofsample_intensity-score}
        %%   \end{subtikzfigure}
        %%   %% \caption{\small Testing Predictions}
        %% \end{tikzfigure}


        }

        \column{0.20}

        \block[bodyoffsety=1.25cm, titleoffsety=1.25cm]{Conclusion}
        {In conclusion, we were able to make a functional AI stress monitor that can detect
        if someone is considered to be at a high stress risk. Through two ways of extracting data
        we identified that the Bag of Words approach was best when it came to accurately preparing the data to
        predict if someone was considered highly stressed or not. The largest takeaway of SAM is its ability
        to help friends and professionals intervene before suicide occurs.\\

        \textbf{Limitations and Errors}

        Our current out-of-sample performance is that SAM will
        incorrectly think that someone ``is
        fine'' about 7\% of the time. To make our model more accurate, we would like to include other risk
        factors such as aggression, depression, hopelessness, impulsiveness, and including our confidence
        column. More parameters will increase SAM's accuracy and make SAM more usefull to
        healthcare professionals. Hyperparameter tuning is also needed, fine tuning the
        values that control SAM's learning processes.\\

        \textbf{Future Work}

        Given time we would like to expand this project to include other emotions conveyed through
        text. Including, having SAM keep a running tab on how often a certain user shows
        high risk signs through multiple posts and the number of times they post. We would also
        like to add a web-based application that can take images and screen shots, as well as text. It
        could also have a user type answers to questions about how they are feeling. The text would
        then be fed to SAM for classification of stress level.
        }

        \block[bodyoffsety=1cm, titleoffsety=1cm]{References}
        {
        \begin{enumerate}
            \normalsize
        \item Ahr. America's Health TRankings. (n.d.). Retrieved from: https://www.americashealthrankings.org/explore/annual
        /measure/Suicide/population/suicide\_15--24/state/NM
        \item Suicide rates by state 2023. (n.d.). Retrieved from
          https://worldpopulationreview.com/state-rankings/suicide-rates-by-state
        \item Suicide statistics and facts.
        SAVE. (n.d.). Retrieved from https://save.org/about-suicide/suicide-statistics/
        \item Turcan, E., & McKeown, K. (2019). Dreaddit: A reddit dataset for stress analysis in social
        media. arXiv preprint arXiv:1911.00133.
        \end{enumerate}
        }
    \end{columns}


\end{document}
