# SAmonitor
samonitor

S.A. Monitor

## motivation

## get going

Clone with ():

git clone https://codeberg.org/EmleeTaylorBowlin/SAMonitor.git

Make sure you have these downloaded:

Pandas(https://pypi.org/project/pandas/)

pip install pandas

numpy(https://numpy.org/install/)

pip install numpy

matplotlib (https://matplotlib.org/stable/users/installing/index.html)

python -m pip install -U pip
python -m pip install -U matplotlib

scikitlearn (https://scikit-learn.org/stable/install.html)

pip install -U scikit-learn
 
All code can be found in, /code/SAM
All documents like, a Final paper and slides can be found in /documents

To run the code:

SAM can be run with
./SAMonitor.py

You can specifiy the feature type and data sets with command line arguments

It is also good to note that SAM has a -help option if you get lost

The basic formula for running SAM is:

./SAMonitor.py --features-type data_processing_type data_file

There are two data_processing_types,
They are intensity-score and bag-of-words. For more information about how they word see page 6 of the final report.

There are two data_sets
They are First20.csv and Third60.csv. First20.csv contians the first 20% of our data set, and Third60 contians the last 60%. For more information about where the data came from, and how it was preped see page 4 of the Final report.

You can also run:
./SAMonitor.py TnPData/dreddit-train.csv TnPData/dreddit-test.csv

This will train SAM on the Third60.csv data set, with the bag-of-words method, the SAM will try himself on a text snippet writen by Mark Galassi.

If you want to try your own text snippet:

(This section is still in progress. Check back later!) 

