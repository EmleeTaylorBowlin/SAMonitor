#! /usr/bin/env python3

import pprint
import csv
import os
import sys
import argparse

# Pandas and numpy for data manipulation
import pandas as pd
import numpy as np
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None
pd.set_option('display.max_columns', 60)

# Matplotlib for visualization
import matplotlib.pyplot as plt

# Machine Learning Models
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.linear_model import LinearRegression
# from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

print('Hey! SAMonitor here! We will now train on data and analyze snippets.')

args = None

def main():
    plt.rcParams.update({'font.size': 14})
    handle_command_line_options()

    # trouble_df = load_train_data('TnPData/First20Copy.csv')
    trouble_df = load_train_data(args.train_fpath)
    # trouble_df = trouble_df[:200]
    text_snippets = np.array(trouble_df.text)

    # first load up the training features and train the model
    train_features, labels, vectorizer = extract_features(trouble_df)
    model = train_model(train_features, labels)
    # some diagnostics, like the confusion matrix and the in-sample
    # mean error
    do_model_diagnostics(text_snippets, train_features, labels, model, vectorizer)
    # now do diagnostics to out of sample stuff
    apply_model_to_csv_file(model, args.test_fpath, vectorizer)
    # now do some applications of the trained model to some stark
    # examples
    show_score(model, 'TnPData/WorrisomeMessage.txt', vectorizer)
    show_score(model, 'TnPData/SunnyMessage.txt', vectorizer)
    # before exiting, show the plot; note that it has also been saved
    # to a file, so this can be turned off with the
    # "--no-interactive-plot" option
    if not args.no_interactive_plot:
        plt.show()


def show_score(model, fname, vectorizer):
    """Demonstrates application of the model to a single feature (text
    snippet)."""
    text = open(fname).read().strip()
    score = apply_model_to_single_feature(model, text, vectorizer)
    print(f'APPLY_TO_TEXT: worrisome score: """{text.strip()}"""  =====>'
          + f' {score} i.e. {"DO_WORRY" if score == 1 else "NO_WORRY"}')


def extract_features(df):
    """The features are our numerical summaries of the paragraphs of text.
    Returns the feature vector and the labels, ready to be passed to
    the random forest algorithm.  The feature vector can be either a
    vector of floats (each float is a feature scalar), or a sparse
    matrix with each row being a feature vector.  The labels are a
    list of 0 or 1 values, indicating "cause for concern" or "no cause
    for concern".
    """
    vectorizer = None
    if args.feature_type == 'bag-of-words':
        Xtrain, labels, vectorizer = make_word_bag(df)
    elif args.feature_type ==  'intensity-score':
        Xtrain, labels = calc_intensity_features(df)
    elif args.feature_type == 'normalized-intensity-score':
        Xtrain, labels = calc_weighted_intensity_features(df)
    else:
        raise Exception(args.feature_type + ' is not a valid feature type')
        # assert(approach in possible_approaches)
    return Xtrain, labels, vectorizer


def make_word_bag(trouble_df):
    """Takes the full data frame and makes a word bag out of the text
    snippets."""
    snippet_list = trouble_df.text
    label_list = trouble_df.label
    vectorizer = TfidfVectorizer(
        sublinear_tf=True, max_df=0.5, min_df=5, stop_words="english",
        ngram_range=(1, 1)
    )
    # vectorizer = CountVectorizer(
    #     # sublinear_tf=True, max_df=0.5, min_df=5, stop_words="english",
    #     max_df=0.5, min_df=5, stop_words="english",
    #     ngram_range=(1, 1)
    # )
    X_train = vectorizer.fit_transform(snippet_list)
    # print(X_train.toarray())
    return X_train, label_list, vectorizer


def calc_intensity_features(df):
    """Simplified preparation of the intensity features.  Just returns
    intensity calculations and labels."""
    feature_array = []
    label_array = []
    for (text_snippet, label) in zip(df.text, df.label):
        intensity = calc_text_intensity(text_snippet)
        if args.verbose:
            print(text_snippet[:60], '=======>', intensity, '-- label:', label)
        feature_array.append(intensity)
        label_array.append(label)
    return np.array(list(zip(feature_array))), np.array(label_array)


def calc_weighted_intensity_features(df):
    """Same as assign_intensities(), but divides each intensity by the
    number of words in the text snippet.
    """
    snippets = df.text
    intensities, labels = calc_intensity_features(df)
    weighted_intensities = intensities[:]
    for i, intense in enumerate(intensities):
        # weigh them down by how many words are in the snippet
        weighted_intensities = intensities / len(snippets[i].split())
    # print('intensities:', intensities)
    # print('weighted_intensities:', weighted_intensities)
    return weighted_intensities, labels


def do_model_diagnostics(text_snippets, Xtrain, labels, model, vectorizer):
    """Uses the model to do some diagnostics on various data samples.
    This should produce the confusion matrix, and the in-sample error
    estimate."""
    prediction_on_training = model.predict(Xtrain)
    if args.verbose:
        print('predict:', Xtrain.shape, prediction_on_training.shape)
    mae_metric = mae(labels, prediction_on_training)
    print('# in-sample error metric (mae):', mae_metric)
    print('# in-sample classifier score:', model.score(Xtrain, labels))
    # now work on the confusion plot
    target_names = ['OK', 'Worry']
    disp = ConfusionMatrixDisplay.from_predictions(labels, prediction_on_training,
                                                   display_labels=target_names,
                                                   normalize='all')
    confusion_fname = f'confusion_insample_{args.feature_type}.png'
    plt.savefig(confusion_fname)
    print(f'# saved confusion matrix to file {confusion_fname}')

def load_train_data(fname):
    """Loads a training data file into a pandas dataframe."""
    print('# loading training data from:', fname)
    df_raw = pd.read_csv(fname, encoding='ISO-8859-1')
    # lower, upper = ['', '']
    # if args.train_slice:
    #     [lower, upper] = args.train_slice.split(':')
    #     df = df[int(lower):int(upper)]
    df_sliced = apply_slice_arg(df_raw, args.train_slice)
    # print('TRAIN_df:', df_sliced[:4])
    if args.verbose:
        print('# train_slice:', df_raw.shape, '-->', df_sliced.shape)
    return df_sliced


def read_kw_map():
    kw2level_map = {'anxiety' : 5,
                    'anxious' : 5,
                    'doom' : 10,
                    'trigger' : 20,
                    'stress' : 5,
                    'losing motivation' : 10,
                    'losing' : 5,
                    'struggling' : 5,
                    'busy' : 5,
                    'sad' : 5,
                    'angry' : 5,
                    'scared' : 5,
                    'afraid' : 5,
                    'despair' : 5,
                    'desperate' : 5,
                    'fear' : 5,
                    'pain' : 5,
                    'trauma' : 5,
                    'helpless' : 5,
                    'scared' : 5,
                    'lost' : 5,
                    'empty' : 5,
                    'worthless' : 5,
                    'burn out' : 5,
                    'burnt out' : 5,
                    'crisis' : 5,
                    #lets look for mental illness
                    'depression' : 5,
                    'PTSD' : 5,
                    'bipolar' : 5,
                    'insomnia' : 5, 
                    #lets look for actions
                    'domestic' : 5,
                    'violence' : 5,
                    'suicide' : 5,
                    'break-up' : 5,
                    'die' : 5,
                    'death' : 5,
                    'isolation' : 5,
                    'traumatised' : 5,
                    'divorce' : 5,
                    'abused' : 5,
                    'abuse' : 5,
                    #lets look for pepole 
                    'boyfriend' : 5,
                    'girlfriend' : 5,
                    'mom' : 5,
                    'dad' : 5,
                    #lets look for adictions 
                    'drugs' : 5,
                    'alcohol' : 5,
                    'alcoholism' : 5,
                    'alcoholic' : 5,
                    #lets look for bad places
                    'jail' : 5,
                    'hospitial' :5,
                    #lets look for items
                    'firearm' : 5,
                    'rope' : 5,
                    'posion' : 5,
                    'gun' : 5,
                    #lets look for phrases
                    'mom has no idea' : 5,
                    'dad has no idea' : 5,
                    'they have no idea' : 5,
                    "I'm fine" : 5,
                    "I don't know" : 5,
                    'idk' : 5,
                    'reached the end' : 5,
                    'personality disorder' : 5}
    return kw2level_map


def calc_text_intensity(txt_orig):
    """scans for keywords..."""
    kw2level_map = read_kw_map()
    txt = txt_orig.lower()
    #Lets look for emotions
    total_intensity = 0
    for key in kw2level_map:
        # see if our keyword (or keyphrase) appears in the text
        if txt.find(key) != -1:
            match_count = txt.count(key)
            total_intensity += match_count * kw2level_map[key]
    return total_intensity


def apply_model_to_single_feature(model, text, vectorizer):
    """Applies our model to a text snippet and returns a score."""
    if args.feature_type == 'bag-of-words':
        feature = vectorizer.transform([text])
        if args.verbose:
            print('feature:', feature.toarray()[0][:60], '...')
        score = model.predict(feature)
    else:
        feature = text2feature(text)
        score = model.predict(np.array([feature]).reshape(-1, 1))
    return score


def apply_model_to_csv_file(model, fpath, train_vectorizer):
    """Applies our model to a whole CSV file.  This is typically for use
    with out-of-sample data."""
    file_df_raw = pd.read_csv(fpath, encoding='ISO-8859-1')
    file_df = apply_slice_arg(file_df_raw, args.test_slice)
    # print('TEST_file_df:', file_df[:4])
    if args.feature_type == 'bag-of-words':
        file_features = train_vectorizer.transform(file_df.text)
        predictions_on_file = model.predict(file_features)
    else:
        file_features = np.array(list(zip([calc_text_intensity(text)
                                           for text in file_df.text])))
        # print('file_features:', np.array(list(zip(file_features))))
        # print('file_features:', np.array(list(zip(file_features))).shape)
        predictions_on_file = model.predict(file_features)
        # predictions_on_file = model.predict(file_features)
    print('# ========= out of sample =========')
    if args.verbose:
        print('# test_slice:', file_df_raw.shape, '-->', file_df.shape)
    target_names = ['OK', 'Worry']
    disp = ConfusionMatrixDisplay.from_predictions(file_df.label, predictions_on_file,
                                                   display_labels=target_names,
                                                   normalize='all')
    confusion_fname = f'confusion_outofsample_{args.feature_type}.png'
    plt.savefig(confusion_fname)
    mae_metric = mae(file_df.label, predictions_on_file)
    print('# out-of-sample error metric (mae):', mae_metric)
    print('# out-of-sample classifier score:', model.score(file_features, file_df.label))


def text2feature(text):
    """The simplest calculation of a feature (scalar or vector) for a
    given text snippet."""
    if args.feature_type == 'intensity-score':
        feature = calc_text_intensity(text)
        # print('FEATURE:', feature)
    elif args.feature_type == 'bag-of-words':
        # vectorizer = TfidfVectorizer(
        #     sublinear_tf=True, max_df=0.5, min_df=5, stop_words="english"
        # )
        # feature = vectorizer.fit_transform([text])
        # print('feature.to_array():', feature.to_array())
        feature = [text]
    elif args.feature_type == 'normalized-intensity-score':
        feature = calc_text_intensity(text) / len(text.split())
    else:
        # assert(approach in possible_approaches)
        raise Exception(args.feature_type + ' is not a valid feature type')
    return feature


def mae(y_true, y_pred):
    """MAE is mean absolute error"""
    return np.mean(abs(y_true - y_pred))


def train_model(train_features, labels):
    """Trains a model based on the feature vector we give it.  Returns
    the model created by scikit learn."""
    if args.verbose:
        print('# training with feature type', args.feature_type)
    if args.feature_type == 'intensity-score':
        model = train_model_intensities(train_features, labels)
    elif args.feature_type == 'bag-of-words':
        model = train_model_word_bag(train_features, labels)
    elif args.feature_type == 'normalized-intensity-score':
        model = train_model_intensities(train_features, labels)
    else:
        raise Exception(args.feature_type + ' is not a valid feature type')
    return model


def train_model_intensities(intensities, labels):
    """Makes a random forest model trained on the intensity feature
    vector."""
    if args.verbose:
        print('TRAIN:', intensities.shape, np.array(list(zip(labels))).shape)
        print('TRAIN:', intensities.shape, labels.shape)
    rf_model = RandomForestClassifier(random_state=60)
    estimator = rf_model.fit(intensities, labels)
    # I don't think we use the estimator returned value; we just use
    # the fact that the random forest model has now been fit
    return rf_model


def train_model_word_bag(train_features, labels):
    """Trains the random forest model on the word bag data."""
    # rf_model = RandomForestRegressor(random_state=60)
    rf_model = RandomForestClassifier(random_state=60)
    # Train the model
    # estimator = rf_model.fit(train_features, np.ravel(list(zip(labels))))
    if args.verbose:
        print(train_features.shape, labels.shape, np.squeeze(list(zip(labels))).shape)
    estimator = rf_model.fit(train_features, np.squeeze(list(zip(labels))))
    # estimator = model.fit(np.ravel(intensities), np.ravel(labels))
    return rf_model


# # Takes in a model, trains the model, and evaluates the model on the test set
# def fit_and_evaluate_word_bag(model, word_bag, labels):
#     if args.verbose:
#         print(word_bag.shape)
#         print(labels.shape)
#     # Train the model
#     estimator = model.fit(word_bag, list(zip(labels)))
#     # estimator = model.fit(np.ravel(intensities), np.ravel(labels))
#     # Now that we have trained and gotten an estimator, we can apply
#     # the trained model to some other data
#     # print('predict:', model.predict([[5, 15, 45, 0, 40, 0, 20],]))
#     prediction_results = model.predict(word_bag)
#     print('predict:', prediction_results)
#     # print('score:', model.score([intensities,], [labels,]))
#     return prediction_results

# # Takes in a model, trains the model, and evaluates the model on the test set
# def fit_and_evaluate_intensities(model, intensities, labels):
#     # Train the model
#     estimator = model.fit(list(zip(intensities)), list(zip(labels)))
#     # estimator = model.fit(np.ravel(intensities), np.ravel(labels))
#     # Now that we have trained and gotten an estimator, we can apply
#     # the trained model to some other data
#     # print('predict:', model.predict([[5, 15, 45, 0, 40, 0, 20],]))
#     prediction_results = model.predict(list(zip(intensities)))
#     if args.verbose:
#         print('predict:', prediction_results)
#     # print('score:', model.score([intensities,], [labels,]))
#     return prediction_results

def handle_command_line_options():
    """Parse command line options and store them in the global args
    object."""
    parser = argparse.ArgumentParser()
    parser.add_argument('train_fpath', nargs='?',
                        default='TnPData/First20.csv',
                        help='path to training data file (csv format)')
    parser.add_argument('--train-slice', type=str)
    parser.add_argument('test_fpath', nargs='?',
                        default='TnPData/Second20.csv',
                        help='path to testing (out-of-sample) data file (csv format)')
    parser.add_argument('--test-slice', type=str)
    parser.add_argument('-x', '--feature-type', type=str,
                        choices=['intensity-score', 'bag-of-words',
                                 'normalized-intensity-score'],
                        default='bag-of-words',
                        help='approach to feature extraction')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='give verbose output')
    parser.add_argument('--no-interactive-plot', action='store_true',
                        help='do not show the interactive plot')
    global args
    args = parser.parse_args()


def apply_slice_arg(the_seq, slice_arg_str):
    """Apply a string of the form 'lower:upper' to a sequence.  This will
    usually come from the command line options for slicing the
    string."""
    if not slice_arg_str:
        return the_seq
    [lower, upper] = slice_arg_str.split(':')
    if lower and upper:
        result = the_seq[slice(int(lower), int(upper))]
    elif lower:
        result = the_seq[slice(int(lower),len(the_seq))]
    elif upper:
        result = the_seq[slice(0, int(upper))]
    else:
        result = the_seq
    return result


if __name__ == '__main__':
    main()
