`dreaddit-train.csv` and `dreaddit-test.csv` come directly from this
repo:

git clone git@github.com:gillian850413/Insight_Stress_Analysis.git

The other `.csv` files have various hand-crafted splits of the big
data file, and several columns removed.  It seems that we can also
directly use these dreaddit files with their full range of collumns,
so I'm not sure why we need the preprocessed files.
